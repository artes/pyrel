
import gameMap
import things.terrain.terrainLoader
import things.creatures.player
import things.creatures.creatureLoader
import things.items.itemLoader

def makeMap(width, height):
    newMap = gameMap.GameMap(width, height)
    wallFactory = things.terrain.terrainLoader.getTerrainFactory('granite wall')
    for x in xrange(width):
        wallFactory.makeTerrain(newMap, (x, 0), 0)
        wallFactory.makeTerrain(newMap, (x, height - 1), 0)
    for y in xrange(height):
        wallFactory.makeTerrain(newMap, (0, y), 0)
        wallFactory.makeTerrain(newMap, (width - 1, y), 0)
    tileFactory = things.terrain.terrainLoader.getTerrainFactory('decorative floor tile')
    tileFactory.makeTerrain(newMap, (10, 10), 0)
    things.terrain.terrainLoader.makeTerrain('door', newMap, (9, 10), 0)

    things.creatures.creatureLoader.makeCreature("Filthy street urchin", newMap, (6, 6))
    things.items.itemLoader.makeItem(('sword', '& Dagger~'), 0, newMap, (3, 4))
    things.items.itemLoader.makeItem(('sword', '& Dagger~'), 0, newMap, (3, 6))
    things.items.itemLoader.makeItem(('amulet', 'Devotion'), 80, newMap, (2, 4))
    things.items.itemLoader.makeItem(('ring', 'Prowess'), 80, newMap, (2, 5))
    things.items.itemLoader.makeItem(('spike', '& Iron Spike~'), 0, newMap, (3, 3))
    things.items.itemLoader.makeItem(('potion', 'Cure Light Wounds'), 0, newMap, (3, 2))
    player = things.creatures.player.Player(newMap, (4, 4))
    playerTemplate = things.creatures.creatureLoader.getFactory('<player>')
    playerTemplate.applyAsTemplate(player)
    player.name = '<player>'
    player.subtype = '<player>'
    return newMap

