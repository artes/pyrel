## Artist that imitates an ASCII-mode drawing style.

import artist
import events
import container
import gui.colors
import gui.flavors

## Ordered list of what we want to draw -- the first thing to match gets 
# displayed.
DRAW_ORDER = [container.UPDATERS, container.ITEMS, container.TERRAIN]

class AsciiArtist(artist.Artist):
    def __init__(self, gameMap):
        artist.Artist.__init__(self, gameMap)
        ## List of [(position, symbol, color)] tuples to draw on top of 
        # everything else.
        self.overlayData = None
        # Get the size of all ASCII characters, so we know how big a single
        # tile in the view is.
        biggestChar = self.getBiggestCharacterDimensions()
        # Tile location to center display at
        self.centerTile = None
        ## Number of horizontal pixels to dedicate to each character we draw.
        # \todo Is casting to int here appropriate? We could be losing up to 
        # a pixel in each dimension...
        self.charWidth = int(biggestChar[0])
        ## Number of vertical pixels to dedicate to each character we draw.
        self.charHeight = int(biggestChar[1])
        ## Number of columns of characters to draw. Recalculated each time
        # self.draw is called.
        self.numColumns = None
        ## Number of rows, ditto.
        self.numRows = None
        ## Precalculated bounding box (xMin, xMax, yMin, yMax); we adjust
        # this if necessary when self.draw is called. Measured in characters.
        self.boundingBox = None
        ## Set of dirty cells -- cells that have been modified since the last
        # time we drew them.
        self.dirtyCells = set()
        self.gameMap.setDisplayFunc(self.setCellDisplay)
   
        events.subscribe("center point for display", self.updateCenterTile)

    ## Mark a cell as being dirtied.
    def setCellDisplay(self, cell, item, wasItemAdded):
        if self.boundingBox is None:
            # Haven't drawn anything yet, so this is irrelevant.
            return
        x, y = cell.pos
        xMin, xMax, yMin, yMax = self.boundingBox
        if x < xMin or x > xMax or y < yMin or y > yMax:
            # Tile isn't actually visible, so we don't care.
            return
        self.dirtyCells.add(cell.pos)


    ## Return a (symbol, color) tuple indicating what to draw at the specified
    # map location.
    def getDisplayData(self, cellPos):
        cX, cY = cellPos
        # Find the highest-priority item in the cell and set that as the 
        # thing to be drawn.
        allThings = self.gameMap.getContainer(cellPos)
        if not allThings:
            # Nothing there; draw a . instead.
            return ('.', 'WHITE')
        bestThing = None
        bestLayerIndex = None
        layers = set(DRAW_ORDER)
        for thing in allThings:
            memberships = self.gameMap.getMembershipsFor(thing)
            for layer in layers.intersection(memberships):
                layerIndex = DRAW_ORDER.index(layer)
                if (bestThing is None or
                        (layerIndex < bestLayerIndex and layer in memberships)):
                    bestThing = thing
                    bestLayerIndex = layerIndex
            if bestLayerIndex == 0:
                # Not going to get better than this.
                break

        if bestThing is not None:
            symbol = bestThing.display['ascii']['symbol']
            color = bestThing.display['ascii']['color']
            if color == 'flavor':
                # Get the color based on the flavor of the item.
                color = gui.flavors.getColorNameForFlavor(bestThing.flavor)
                color = gui.colors.getColorByName(color)
            # \todo Is there a better way to do this check?
            elif type(color) in [str, unicode]:
                # Map from a color name to the color tuple.
                color = gui.colors.getColorByName(color)
            return (symbol, color)


    ## Draw the game view ASCII-style (i.e. one character per tile).
    # \param dc Whatever drawing context is needed to draw the game view.
    # \param pixelWidth Width in pixels of the display.
    # \param pixelHeight Height in pixels of the display.
    # \param curPrompt A Prompt instance to draw on top of the game view (or
    #        None if there is no Prompt).
    # \param shouldRedrawAll True if we need to force a full-display refresh.
    def draw(self, dc, pixelWidth, pixelHeight, curPrompt, shouldRedrawAll):
        raise RuntimeError("ASCII Artist [%s] didn't implement its draw function" % str(type(self)))


    ## Draw a single character of the given color at the specified 
    # character coordinates (not pixel coordinates).
    def drawChar(self, dc, symbol, color, x, y):
        raise RuntimeError("ASCII Artist [%s] didn't implement its drawChar function" % str(type(self)))


    ## Draw the main game view -- a character for each tile in the map.
    def drawMap(self, dc, shouldRedrawAll):
        if self.centerTile is None:
            xMin, yMin = self.getUpperLeftCorner()
        else:
            xMin = max(0, self.centerTile[0] - self.numColumns / 2)
            yMin = max(0, self.centerTile[1] - self.numRows / 2)
 
        xMax = min(self.gameMap.width, xMin + self.numColumns)
        yMax = min(self.gameMap.height, yMin + self.numRows)
        
        #avoid overflowing
        if (xMin + self.numColumns > self.gameMap.width):
            xMin = self.gameMap.width - self.numColumns
        if (yMin + self.numRows > self.gameMap.height):
            yMin = self.gameMap.height - self.numRows

        if self.boundingBox != (xMin, xMax, yMin, yMax) and not shouldRedrawAll:
            # The bounding box has changed, either because the window resized
            # or because the screen has scrolled. Odds are that the screen
            # has scrolled, so we can save some effort by just redrawing
            # the current screen, shifted over a bit. 
            xOffset = xMin - self.boundingBox[0]
            yOffset = yMin - self.boundingBox[2]
            self.copyMapTo(dc, -xOffset, -yOffset)
            # Find tiles that weren't covered by this action, and mark them
            # as dirty. If we're below / to the right of where we used to be
            # (x/y offsets > 0) then the newly-exposed tiles are on the 
            # bottom and right borders, and vice versa.
            xVals = range(xMax - xOffset, xMax)
            yVals = range(yMax - yOffset, yMax)
            if xOffset < 0:
                xVals = range(xMin, xMin - xOffset)
            if yOffset < 0:
                yVals = range(yMin, yMin - yOffset)
            for x in xVals:
                self.dirtyCells.update([(x, y) for y in xrange(yMin, yMax)])
            for y in yVals:
                self.dirtyCells.update([(x, y) for x in xrange(xMin, xMax)])

        iterator = self.dirtyCells
        if shouldRedrawAll:
            iterator = ((x, y)
                    for x in xrange(xMin, xMax)
                    for y in xrange(yMin, yMax))

        for x, y in iterator:
            if x < xMin or x > xMax or y < yMin or y > yMax:
                continue
            symbol, color = self.getDisplayData((x, y))
            self.drawChar(dc, symbol, color, x - xMin, y - yMin)
        self.boundingBox = (xMin, xMax, yMin, yMax)
        self.dirtyCells.clear()


    ## Display a one-line string at the provided position.
    # The color is a color name, and not a UI-native color
    def writeString(self, dc, colorName, xPos, yPos, string):
        color = gui.colors.getColorByName(colorName)
        for i, char in enumerate(string):
            self.drawChar(dc, char, color, xPos + i, yPos)


    ## Display a sequence of string pairs, where the first item in each pair
    # is left-aligned and the second item is right-aligned, with blank space
    # in between them. 
    def drawStrings(self, dc, strings, yOffset, minFirstLength = 20):
        if not strings:
            return
        longestFirst = max(minFirstLength, max([len(s[0]) for s in strings]))
        longestSecond = max([len(s[1]) for s in strings])

        # Need at least one space between the items.
        maxLen = longestFirst + longestSecond + 1
        for i, (first, second) in enumerate(strings):
            # First entry and spacer.
            spacer = ' ' * (maxLen - len(first) - len(second))
            self.writeString(dc, 'WHITE', self.numColumns - maxLen - 1,
                    i + 1, "%s%s" % (first, spacer))
            # Second entry.
            self.writeString(dc, 'WHITE', self.numColumns - len(second) - 1, 
                    i + 1, second)

    ## Update the location of the display
    def updateCenterTile(self, location):
        self.centerTile = location
    
    
    ## Return the tile that's in the upper-left corner.
    def getUpperLeftCorner(self):
        # Center the display on the player...
        playerPos = self.gameMap.getContainer(container.PLAYERS)[0].pos
        xOffset = max(playerPos[0] - self.numColumns / 2, 0)
        yOffset = max(playerPos[1] - self.numRows / 2, 0)
        # ...but if this would have us running off the end of the map in 
        # one direction, pull the center back towards the other corner.
        xOffset -= max(0, xOffset + self.numColumns - self.gameMap.width)
        yOffset -= max(0, yOffset + self.numRows - self.gameMap.height)
        xOffset = max(0, xOffset)
        yOffset = max(0, yOffset)
        return (xOffset, yOffset)


    ## Return a bounding box (x, y, width, height) describing the screen
    # position of the given tile.
    def getTileBox(self, pos):
        xOffset, yOffset = self.getUpperLeftCorner()
        dx = pos[0] - xOffset
        dy = pos[1] - yOffset
        return (dx * self.charWidth, dy * self.charHeight, 
                self.charWidth, self.charHeight)


    ## Retrieve our character dimensions.
    def getCharSize(self):
        return (self.charWidth, self.charHeight)


    ## Receive a new list of [(position, symbol, color)] overlay data to draw.
    # Or receive None to disable overlay drawing.
    def setOverlay(self, newData):
        self.overlayData = newData


    ## Return the cell at the specified pixel coordinates.
    def getCellAt(self, x, y):
        xOffset, yOffset = self.getUpperLeftCorner()
        return (xOffset + x / self.charWidth, yOffset + y / self.charHeight)
