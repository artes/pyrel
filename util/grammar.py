# coding=utf-8
## Grammar utilities

import string

# Constants:

## Not all these letters are actually in the English language, but what with all the borrowed words...
vowels = u'aeiouàáâãäåæèéêëìíîïðòóôõöøùúûüāăąēĕėęěĩīĭįıĳōŏőœũūŭůűų'


## Generate the appropriate plural form of the given word, based on the quantity.
# Formatting for names to account for plurality:
# 
# sheep
# horse~
# knife:knives
# 
# 'sheep' doesn't get modified when plural.  Returns same value regardless of quantity.
# 'horse~' appends an 's' in place of the ~ when plural.  1 = 'horse', 2+ = 'horses'
# 'knife:knives' explicity states the plural form.  Anything before the colon for 1, anything after for 2+.
def makePlural(basename, quantity):
    if not isinstance(basename, basestring):
        raise RuntimeError("Provided basename is not a string.")
    if not isinstance(quantity, int):
        raise RuntimeError("Provided quantity is not a number.")
    if quantity < 0:
        raise RuntimeError("Invalid quantity: %d" % quantity)

    if u'~' in basename:
        if quantity == 1:
            return basename.replace(u'~', u'')
        return basename.replace(u'~', u's')
    elif u':' in basename:
        parts = basename.split(u':')
        if quantity == 1 or len(parts) == 1:
            return parts[0]
        return parts[1]
    else:
        return basename


## Get the article to use with the provided word phrase, based on quantity and
#  whether we're using the definite article or not.
# \param phrase - the word phrase the article is being applied to
# \param quantity - the quantity of the word in question
# \param shouldUseDefiniteArticle - indicate whether to use a definite rather than
#       indefinite article
def getArticle(phrase, quantity = 1, shouldUseDefiniteArticle = False):
    if not isinstance(phrase, basestring):
        raise RuntimeError("Provided phrase is not a string.")
    if not isinstance(quantity, int):
        raise RuntimeError("Provided quantity is not a number.")
    if quantity < 0:
        raise RuntimeError("Invalid quantity: %d" % quantity)

    # In the case of quantity 0, the 'article' to be used depends on
    # the message context.  We therefore don't specify anything here.
    if quantity == 0:
        return u''
    elif quantity > 1:
        return unicode(quantity);

    if shouldUseDefiniteArticle:
        return u'the'

    # \todo - This won't always return the proper form for a word starting
    # with a vowel (eg: uranium).
    if phrase[0].lower() in vowels:
        return u'an'
    else:
        return u'a'


##Get a fully constructed item name based on the provided parts of speech.
# \param baseNoun - The fundamental noun of the item
# \param baseAdjectives - List of adjectives to apply to the baseNoun
# \param modifierNoun - A noun that can modify the base noun (and should
#        be placed before the baseAdjectives)
# \param modAdjectives - List of adjectives to apply to the modifierNoun
#        (or just extend the number being applied to the base noun)
# \param itemName - The proper 'name' of the item. Placed after the base noun
# \param suffixList - List of suffixes to add after the base noun, if
#        the item doesn't have a proper name.
# \param quantity - The quantity of the item, which determines the article
# \param unique - Whether the item should be considered unique
def getFullItemName(baseNoun, baseAdjectives = [],
                    modifierNoun = None, modAdjectives = [],
                    suffixList = [], quantity = 1, isUnique = False):
    phrase = u""

    for adj in modAdjectives:
        phrase += u"%s " % string.capwords(adj)

    if modifierNoun:
        pluralModNoun = makePlural(modifierNoun, quantity).capitalize()
    else:
        pluralModNoun = None

    if pluralModNoun:
        phrase += u"%s " % pluralModNoun

    for adj in baseAdjectives:
        phrase += u"%s " % string.capwords(adj)

    phrase += u"%s" % string.capwords(makePlural(baseNoun, quantity))

    # Special consideration:
    # If the last item in the suffix list starts with an ampersand (&),
    # move the 'and' addition to the next-to-last entry, and append
    # the last item without additional modifiers (after removing the &).
    # This is for dealing with things like 'a scroll of Light titled "xyz"'.
    if suffixList:
        if suffixList[-1][0] == u"&":
            mainList = suffixList[0:-1]
            finalItem = suffixList[-1][1:]
        else:
            mainList = suffixList
            finalItem = None

        if mainList:
            if mainList[0][0].isalpha():
                phrase += u" of"
            phrase += u" %s" % mainList[0]
            for suffix in mainList[1:-1]:
                phrase += u", %s" % suffix
            if len(mainList) > 1:
                phrase += u" and %s" % mainList[-1]

        if finalItem:
            phrase += u" %s" % finalItem

    article = getArticle(phrase, quantity, isUnique)

    phrase = u"%s %s" % (article, phrase)

    return phrase


