## This module holds utility functions for dealing with threads.

import threading


## Decorator function to cause the wrapped function to be called in a new
# thread.
def callInNewThread(func):
    def wrappedFunc(*args, **kwargs):
        thread = threading.Thread(target=func, args=args, kwargs=kwargs)
        thread.start()
        return thread
    return wrappedFunc
